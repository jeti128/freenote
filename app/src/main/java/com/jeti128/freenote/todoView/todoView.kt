package com.jeti128.freenote.todoView

import android.content.ClipData.Item
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import com.jeti128.freenote.noteView.NoteViewModel
import com.jeti128.freenote.todoItemView.todoItemView

@Composable
fun todoView(
    navController: NavController,
    backStackEntry: NavBackStackEntry,
    tvm: TodoViewModel = viewModel()
) {
    val states by tvm.uiState.collectAsState();
    val id: Int = backStackEntry.arguments?.getString("id")?.toInt() ?: 0;

    if (states.id == 0 && states.id != id) {
        tvm.get(id);
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    )
    {
            OutlinedTextField(
                value = states.title,
                label = { Text("Title") },
                onValueChange = { newText ->
                    tvm.updateField(newText)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 5.dp, end = 5.dp, top = 5.dp, bottom = 5.dp)

            )
            todoItemView(navController = navController, todoRootId = states.id, category = "todo", updateTodo = { tvm.save() })
    }
}