package com.jeti128.freenote.todoView

import android.app.Application
import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jeti128.freenote.db.*
import com.jeti128.freenote.noteView.NoteViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import java.util.*

class TodoViewModel(application: Application) : AndroidViewModel(application)  {
    private val repository: TodoRepository;
    private val itemRepository : ItemRepository;
    private var saveInProgress: Boolean = false;
    private var lastSavedState: String = "";
    private val _uiState = MutableStateFlow(
        todoViewState()
    )
    val uiState: StateFlow<todoViewState> = _uiState.asStateFlow()

    init {
        val todoDao = AppDB.getDatabase(application).TodoDBDao()
        repository = TodoRepository(todoDao)
        val itemDoa = AppDB.getDatabase(application).ItemDBDao();
        itemRepository = ItemRepository(itemDoa);
    }

    fun get(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getIdFromRoom(id).collect { item ->
                if (_uiState.value.id != item.id) {
                    _uiState.value = todoViewState(
                        id = item.id,
                        title = TextFieldValue(item.title),
                    );
                    lastSavedState = item.title
                    itemRepository.getItemsToMain("todo", id).collect { items ->
                        _uiState.update { currentState ->
                            currentState.copy(
                                id = currentState.id,
                                title = currentState.title,
                            )
                        }
                    }
                }
            }
        }
    }

    fun updateField(newValue: TextFieldValue) {
        _uiState.update { currentState ->
            currentState.copy(
                id = currentState.id,
                title = newValue,
            )
        }

        viewModelScope.launch(Dispatchers.IO) {
            delay(2000)
            if (lastSavedState != newValue.text && _uiState.value.title.text == newValue.text) {
                save()
            }
        }
    }

    suspend fun save() {
        if (_uiState.value.id == 0) {
            val newId: Long = repository.insert(Todo(_uiState.value.id, _uiState.value.title.text, Date()));
            _uiState.update { currentState ->
                currentState.copy(
                    id = newId.toInt(),
                    title = currentState.title,
                )
            }
        } else {
            repository.updateInRoom(Todo(_uiState.value.id, _uiState.value.title.text, Date()))
        }
    }
}