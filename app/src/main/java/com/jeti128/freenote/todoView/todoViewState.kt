package com.jeti128.freenote.todoView

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.ui.text.input.TextFieldValue

data class todoViewState(
    val id: Int = 0,
    val title: TextFieldValue = TextFieldValue(""),
)