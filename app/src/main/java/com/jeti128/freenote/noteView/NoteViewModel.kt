package com.jeti128.freenote.noteView

import android.app.Application
import android.util.Log
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.jeti128.freenote.MainView.MainViewState
import com.jeti128.freenote.db.AppDB
import com.jeti128.freenote.db.Note
import com.jeti128.freenote.db.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.util.*

class NoteViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: NoteRepository;
    private var saveInProgress: Boolean = false;
    private var lastSavedState: String = "";
    private val _uiState = MutableStateFlow(
        NoteViewState()
    )
    val uiState: StateFlow<NoteViewState> = _uiState.asStateFlow()

    init {
        val noteDao = AppDB.getDatabase(application).NoteDBDao()
        repository = NoteRepository(noteDao)
    }

    fun get(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getIdFromRoom(id).collect { item ->
                if (_uiState.value.id != item.id) {
                    _uiState.value = NoteViewState(
                        item.id,
                        TextFieldValue(item.title),
                        TextFieldValue(item.note)
                    );
                    lastSavedState = item.note
                }
            }
        }
    }

    suspend fun updateState(newValue: TextFieldValue, isNew: Boolean) {
        var newItemid: Long = 0;
        if (isNew) {
            newItemid = repository.insert(
                Note(
                    id = 0,
                    title = "",
                    note = newValue.text,
                    Date()
                )
            )
        } else {
            newItemid = _uiState.value.id.toLong();
            repository.updateInRoom(
                Note(
                    _uiState.value.id,
                    title = "",
                    note = newValue.text,
                    Date()
                )
            )
        }

        lastSavedState = _uiState.value.note.text
    }

    fun updateNoteField(newValue: TextFieldValue) {
        _uiState.update { currentState ->
            currentState.copy(
                id = currentState.id,
                title = currentState.title,
                note = newValue,
            )
        }


        viewModelScope.launch(Dispatchers.IO) {
            delay(1000)

            // check different for states for save this is protect app for can't do too many not nessecerry save
            if (
                !saveInProgress &&
                _uiState.value.note.text == newValue.text &&
                lastSavedState != newValue.text
            ) {
                saveInProgress = true;

                if (_uiState.value.id == 0) {
                    updateState(newValue, true)
                    saveInProgress = false;
                } else {
                    updateState(newValue, false);
                    saveInProgress = false;
                }
            } else {
                saveInProgress = false;
            }
        }
    }
}

