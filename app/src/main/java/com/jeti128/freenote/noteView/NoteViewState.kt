package com.jeti128.freenote.noteView

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.ui.text.input.TextFieldValue
import com.jeti128.freenote.db.Note

data class NoteViewState(
    val id: Int = 0,
    val title: TextFieldValue = TextFieldValue(""),
    val note: TextFieldValue = TextFieldValue(""),
)
