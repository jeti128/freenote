package com.jeti128.freenote.noteView

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import com.jeti128.freenote.db.Note
import java.util.*

@Composable
fun NoteView(navController: NavController, backStackEntry: NavBackStackEntry, nvm: NoteViewModel = viewModel()){
    val states by nvm.uiState.collectAsState();
    val id: Int = backStackEntry.arguments?.getString("id")?.toInt() ?: 0;

    if (states.id == 0 && states.id != id) {
        nvm.get(id);
    }

    Column(modifier = Modifier.fillMaxWidth().fillMaxHeight()) {
        TextField(
            value = states.note,
            onValueChange = { newText ->
                nvm.updateNoteField(newText)
            },
            modifier = Modifier.fillMaxWidth().fillMaxHeight(),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colors.background
            )
        )
    }
}