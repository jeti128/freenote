package com.jeti128.freenote

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.jeti128.freenote.MainView.MainView
import com.jeti128.freenote.ui.theme.FreenoteTheme
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.URL

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //ViewModelProvider(this).get(TodoItemViewModel::class.java);

        setContent {
            when {
                intent?.action == Intent.ACTION_SEND -> MyApp(intent = intent)
                else -> {
                    MyApp(intent = intent)
                }
            }
        }
    }
}


@Composable
fun MyApp(intent: Intent) {
    FreenoteTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            //color = MaterialTheme.colors.background
        ) {
            if ("text/plain" == intent.type) {
                intent.getStringExtra(Intent.EXTRA_TEXT)?.let { text ->
                    // Update UI to reflect text being shared
                    Log.d("test simple data share", text)
                    MainView(text)
                }

            }
            MainView(null)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    FreenoteTheme {
        MainView(null)
    }
}