package com.jeti128.freenote.MainView

import android.util.Log
import com.jeti128.freenote.R
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.jeti128.freenote.noteListView.NoteListView
import com.jeti128.freenote.noteView.NoteView
import com.jeti128.freenote.sideBarView.SideBarView
import com.jeti128.freenote.todoListView.TodoListView
import com.jeti128.freenote.todoView.todoView
import com.jeti128.freenote.ui.theme.materialBlue700
import kotlinx.coroutines.launch

data class MenuItem(
    val name: String = "",
    val route: String = "/",
    val icon: Int = R.drawable.icon_menu,
    val buttomBar: Boolean = false,
    val showAddButtom: Boolean = true,
    val backButtom: Boolean = false

)

@Composable
fun MainView(
    simpleDataInput: String?,
    mvm: MainViewModel = viewModel()
) {
    val navController = rememberNavController()
    val states by mvm.uiState.collectAsState();
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val scope = rememberCoroutineScope()

    if (!simpleDataInput.isNullOrEmpty()) {
        mvm.saveNote(simpleDataInput);
    }

    val menuItems: MutableList<MenuItem> = mutableListOf(
        MenuItem("TodoList", "todolist", R.drawable.checklist, false, true, false),
        MenuItem("Note", "note", R.drawable.note, false, false, true),
        MenuItem("Notes", "notelist", R.drawable.note, false, true, false),
        MenuItem("Todo", "todo", R.drawable.checklist, false, false, true)
    )

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = {
                    Row() {
                        if (states.backButtom) {
                            Image(
                                painter = painterResource(id = R.drawable.arrow_back),
                                contentDescription = "Back buttom",
                                colorFilter = ColorFilter.tint(Color.White),
                                contentScale = ContentScale.FillWidth,
                                modifier = Modifier
                                    .padding(start = 5.dp, end = 15.dp, top = 10.dp, bottom = 5.dp)
                                    .size(30.dp)
                                    .clickable(
                                        onClick = {
                                            if (states.selectedComponent == "note") {
                                                navController.navigate("notelist")
                                            }
                                            if (states.selectedComponent == "todo") {
                                                navController.navigate("todolist")
                                            }
                                        }
                                    )
                            )
                        }
                        Text(
                            text = states.navBarTitle,
                            color = Color.White,
                            fontSize = 30.sp,
                            fontWeight = FontWeight(600),
                            modifier = Modifier.padding(top = 3.dp)
                        )
                        Spacer(Modifier.weight(1f))
                        Image(
                            painter = painterResource(id = R.drawable.icon_menu),
                            contentDescription = "Home",
                            colorFilter = ColorFilter.tint(Color.White),
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .padding(start = 5.dp, end = 15.dp, top = 5.dp, bottom = 5.dp)
                                .size(40.dp)
                                .clickable(
                                    onClick = {
                                        scope.launch {
                                            scaffoldState.drawerState.apply {
                                                if (isClosed) open() else close()
                                            }
                                        }
                                    }
                                )
                        )
                    }
                },
                backgroundColor = MaterialTheme.colors.primary
            )
        },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            if (states.showAddButtom) {
                FloatingActionButton(onClick = {
                    if (states.selectedComponent == "notelist") {
                        navController.navigate("note")
                    }
                    if (states.selectedComponent == "todolist") {
                        navController.navigate("todo")
                    }
                }) {
                    Text("+")
                }
            }
        },
        drawerContent = { SideBarView(navController) },
        content = {
            NavHost(navController = navController, startDestination = "notelist") {
                composable("notelist") {
                    mvm.update(menuItems[2])
                    NoteListView(navController = navController)
                }
                composable("note?id={id}") { backStackEntry ->
                    mvm.update(menuItems[1])
                    NoteView(navController = navController, backStackEntry = backStackEntry)
                }
                composable("todolist") {
                    mvm.update(menuItems[0])
                    TodoListView(navController = navController)
                }
                composable("todo?id={id}") { backStackEntry ->
                    mvm.update(menuItems[3])
                    todoView(navController = navController, backStackEntry = backStackEntry)
                }
            }
        },
        bottomBar = {
            if (states.buttomBar) {
                BottomAppBar(backgroundColor = materialBlue700) {
                    Text(text = "buttom bar")
                }
            }
        }
    )
}