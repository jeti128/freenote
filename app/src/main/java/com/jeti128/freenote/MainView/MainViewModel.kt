package com.jeti128.freenote.MainView

import android.app.Application
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.jeti128.freenote.db.AppDB
import com.jeti128.freenote.db.Note
import com.jeti128.freenote.db.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.util.*

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: NoteRepository;

    private val _uiState = MutableStateFlow(
        MainViewState()
    )
    val uiState: StateFlow<MainViewState> = _uiState.asStateFlow()

    init {
        val noteDao = AppDB.getDatabase(application).NoteDBDao()
        repository = NoteRepository(noteDao)
    }

    public fun saveNote(note: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(Note(id = 0, title = "", note = note, update = Date()))
        }
    }

    public fun update(selectedMenuItem: MenuItem)
    {
        _uiState.update { currentState ->
            currentState.copy(
                selectedComponent = selectedMenuItem.route,
                buttomBar= selectedMenuItem.buttomBar,
                showAddButtom = selectedMenuItem.showAddButtom,
                navBarTitle = selectedMenuItem.name,
                backButtom = selectedMenuItem.backButtom
            )
        }
    }

}