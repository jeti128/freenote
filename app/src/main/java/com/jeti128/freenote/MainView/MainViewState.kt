package com.jeti128.freenote.MainView

import androidx.compose.runtime.mutableStateListOf
import com.jeti128.freenote.R

data class MainViewState(
    val selectedComponent: String = "notelist",
    val buttomBar: Boolean = false,
    val showAddButtom: Boolean = true,
    val navBarTitle: String = "Todo",
    val backButtom: Boolean = false
)