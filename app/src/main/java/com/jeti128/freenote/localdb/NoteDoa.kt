package com.jeti128.freenote.db

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {
    @Query("SELECT * FROM notes ORDER BY id DESC")
    fun getNotes(): Flow<List<Note>>

    @Query("SELECT * FROM notes WHERE :key = :value")
    fun get(key: String, value: String): Flow<Note>

    @Query("SELECT * FROM notes WHERE id = :id")
    fun getFromId(id: Int): Flow<Note>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addNote(note: Note)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(note: Note): Long

    @Update
    fun updateNote(note: Note)

    @Delete
    fun deleteNote(note: Note)
}