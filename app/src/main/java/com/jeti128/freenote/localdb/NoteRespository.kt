package com.jeti128.freenote.db

class NoteRepository(
    private val noteDao: NoteDao
) : RepositoryInterface<Note> {
    override fun getAllFromRoom() = noteDao.getNotes()

    override suspend fun getFromRoom(key: String, value: String) = noteDao.get(key, value)

    override suspend fun getIdFromRoom(id: Int) = noteDao.getFromId(id)

    override suspend fun addToRoom(note: Note) = noteDao.addNote(note)

    suspend fun insert(note: Note): Long = noteDao.insert(note)

    override suspend fun updateInRoom(note: Note) = noteDao.updateNote(note)

    override suspend fun deleteFromRoom(note: Note) = noteDao.deleteNote(note)
}