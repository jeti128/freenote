package com.jeti128.freenote.db

import android.content.Context
import androidx.room.*
import com.jeti128.freenote.localdb.DateTimeConverter

@Database(entities = [Note::class, Todo::class, Item::class], version = 1, exportSchema = false)
@TypeConverters(DateTimeConverter::class)
abstract class AppDB : RoomDatabase() {
    abstract fun NoteDBDao(): NoteDao;
    abstract fun TodoDBDao(): TodoDao;
    abstract fun ItemDBDao(): ItemDao;

    companion object {
        @Volatile
        private var INSTANCE: AppDB? = null

        fun getDatabase(context: Context): AppDB{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDB::class.java,
                    "AppDB"
                )
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}