package com.jeti128.freenote.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemDao {
    @Query("SELECT * FROM items ORDER BY id ASC")
    fun getAll(): Flow<List<Item>>

    @Query("SELECT * FROM items WHERE :key = :value")
    fun get(key: String, value: String): Flow<Item>

    @Query("SELECT * FROM items WHERE id = :id")
    fun getFromId(id: Int): Flow<Item>

    @Query("SELECT * FROM items WHERE category = :category AND root = :rootId")
    fun getItemsToMain(category: String, rootId: Int): Flow<List<Item>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(item: Item)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(item: Item): Long

    @Update
    fun update(item: Item)

    @Delete
    fun delete(item: Item)
}