package com.jeti128.freenote.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface TodoDao {
    @Query("SELECT * FROM todos ORDER BY id ASC")
    fun getAll(): Flow<List<Todo>>

    @Query("SELECT * FROM todos WHERE :key = :value")
    fun get(key: String, value: String): Flow<Todo>

    @Query("SELECT * FROM todos WHERE id = :id")
    fun getFromId(id: Int): Flow<Todo>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(todo: Todo)

    @Insert
    fun insert(todo: Todo) : Long

    @Update
    fun update(todo: Todo)

    @Delete
    fun delete(todo: Todo)
}