package com.jeti128.freenote.db

import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow

interface RepositoryInterface<R> {

    fun getAllFromRoom(): Flow<List<R>>

    suspend fun getFromRoom(key: String, value: String): Flow<R>

    suspend fun getIdFromRoom(id: Int): Flow<R>

    suspend fun addToRoom(note: R)

    suspend fun updateInRoom(note: R)

    suspend fun deleteFromRoom(note: R)
}