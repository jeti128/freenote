package com.jeti128.freenote.db

class ItemRepository(
    private val itemDao: ItemDao
) : RepositoryInterface<Item> {
    override fun getAllFromRoom() = itemDao.getAll()

    override suspend fun getFromRoom(key: String, value: String) = itemDao.get(key, value)

    override suspend fun getIdFromRoom(id: Int) = itemDao.getFromId(id)

    override suspend fun addToRoom(item: Item) = itemDao.add(item)

    suspend fun insert (item: Item) = itemDao.insert(item)

    override suspend fun updateInRoom(item: Item) = itemDao.update(item)

    override suspend fun deleteFromRoom(item: Item) = itemDao.delete(item)

    suspend fun getItemsToMain(category: String, rootId: Int) = itemDao.getItemsToMain(category, rootId)
}