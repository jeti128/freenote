package com.jeti128.freenote.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "items")
data class Item(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val category: String,
    val root: Int,
    val title: String,
    var status: Boolean
)