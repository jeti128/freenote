package com.jeti128.freenote.db

import java.util.concurrent.Callable

class TodoRepository(
    private val todoDoa: TodoDao
) : RepositoryInterface<Todo> {
    override fun getAllFromRoom() = todoDoa.getAll()

    override suspend fun getFromRoom(key: String, value: String) = todoDoa.get(key, value)

    override suspend fun getIdFromRoom(id: Int) = todoDoa.getFromId(id)

    override suspend fun addToRoom(todo: Todo) = todoDoa.add(todo)

    suspend fun insert(todo: Todo) = todoDoa.insert(todo)

    override suspend fun updateInRoom(todo: Todo) = todoDoa.update(todo)

    override suspend fun deleteFromRoom(todo: Todo) = todoDoa.delete(todo)

}