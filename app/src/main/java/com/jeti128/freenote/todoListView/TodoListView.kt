package com.jeti128.freenote.todoListView

import android.content.Intent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.jeti128.freenote.R

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TodoListView(navController: NavController, tlvm: TodoListViewModel = viewModel()) {
    val states by tlvm.uiState.collectAsState();
    tlvm.getAll()
    val context = LocalContext.current

    if (states.menu) {
        AlertDialog(
            onDismissRequest = { tlvm.openOrClosePopUP(null) },
            title = { Text("Menu") },
            text = {
                Column(modifier = Modifier.fillMaxWidth()) {
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .clickable {
                                tlvm.delete();
                            }
                            .padding(bottom = 15.dp)
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.delete),
                            contentDescription = "Back buttom",
                            colorFilter = ColorFilter.tint(Color.Black),
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .padding(start = 0.dp, end = 10.dp, top = 0.dp, bottom = 0.dp)
                                .size(20.dp)
                        )
                        Text(
                            text = "Delete",
                        )

                    }
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .clickable {
                                val sendIntent: Intent = Intent().apply {
                                    action = Intent.ACTION_SEND
                                    putExtra(Intent.EXTRA_TEXT, states.selected?.title)
                                    type = "text/plain"
                                }

                                val shareIntent = Intent.createChooser(sendIntent, null)
                                context.startActivity(shareIntent)
                            }
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.share),
                            contentDescription = "Back buttom",
                            colorFilter = ColorFilter.tint(Color.Black),
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .padding(start = 0.dp, end = 10.dp, top = 0.dp, bottom = 0.dp)
                                .size(20.dp)
                        )
                        Text(
                            text = "Share",
                        )

                    }
                }
            },
            modifier = Modifier // Set the width and padding
                .fillMaxWidth()
                .padding(12.dp),
            shape = RoundedCornerShape(5.dp),
            backgroundColor = Color.White,
            properties = DialogProperties(
                dismissOnBackPress = true,
                dismissOnClickOutside = true
            ),
            confirmButton = {
            },
            dismissButton = {
            },
        )
    }

    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        items(items = states.todos ?: listOf(), itemContent = { item ->
            var substringlength: Int = 40;
            var date = (item.update.month + 1).toString() + "." + item.update.getDate().toString();
            if (item.update.month < 10) {
                date = "0" + date
            }

            if (item.title.length < 40) {
                substringlength = item.title.length;
            }
            LazyRow() {
                item {
                    Text(
                        item.title.substring(0, substringlength),
                        modifier = Modifier
                            .fillParentMaxWidth(0.85f)
                            .padding(start = 17.dp, top = 15.dp, bottom = 10.dp)
                            //.clickable { navController.navigate("note?id=${item.id}") },
                            .combinedClickable(
                                onClick = { navController.navigate("todo?id=${item.id}") },
                                onLongClick = { tlvm.openOrClosePopUP(item) }
                            ),
                        fontWeight = FontWeight.Bold,
                    )
                }
                item {
                    Text(text = date, modifier = Modifier.padding(top = 5.dp))
                }
            }
            TabRowDefaults.Divider(color = Color.LightGray)
        });
    }

}