package com.jeti128.freenote.todoListView

import android.app.Application
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.jeti128.freenote.MainView.MainViewState
import com.jeti128.freenote.db.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class TodoListViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: TodoRepository;

    private val _uiState = MutableStateFlow(
        TodoListViewState()
    )
    val uiState: StateFlow<TodoListViewState> = _uiState.asStateFlow()

    init {
        val todoDao = AppDB.getDatabase(application).TodoDBDao()
        repository = TodoRepository(todoDao)
    }

    fun getAll() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getAllFromRoom().collect { items ->
                _uiState.update { currentState ->
                    currentState.copy(
                        todos = items,
                        menu = currentState.menu,
                        selected = currentState.selected
                    )
                }
            }
        }
    }

    fun openOrClosePopUP(selected: Todo?) {
        _uiState.update { currentState ->
            currentState.copy(
                todos = currentState.todos,
                menu = !currentState.menu,
                selected = selected
            )
        }
    }

    fun delete() {
        GlobalScope.launch(Dispatchers.IO) {
            if (_uiState.value.selected != null) {
                repository.deleteFromRoom(_uiState.value.selected!!)
                openOrClosePopUP(null)
            }
        }
    }
}