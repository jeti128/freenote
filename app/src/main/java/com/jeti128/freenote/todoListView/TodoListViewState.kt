package com.jeti128.freenote.todoListView

import androidx.compose.runtime.mutableStateListOf
import com.jeti128.freenote.db.Todo

data class TodoListViewState(
    val todos: List<Todo> = mutableStateListOf(),
    val menu: Boolean = false,
    val selected: Todo? = null
)