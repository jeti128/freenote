package com.jeti128.freenote.noteListView


import android.app.Application
import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.jeti128.freenote.db.AppDB
import com.jeti128.freenote.db.Note
import com.jeti128.freenote.db.NoteRepository
import com.jeti128.freenote.noteView.NoteViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class NoteListViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: NoteRepository;

    private val _uiState = MutableStateFlow(
        NoteListViewState()
    )
    val uiState: StateFlow<NoteListViewState> = _uiState.asStateFlow()

    init {
        val noteDao = AppDB.getDatabase(application).NoteDBDao()
        repository = NoteRepository(noteDao)
    }

    fun getAll() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getAllFromRoom().collect { items ->
                _uiState.update { currentState ->
                    currentState.copy(
                        notes = items,
                        menu = currentState.menu,
                        selectedNote = currentState.selectedNote
                    )
                }
            }

        }
    }

    fun openOrClosePopUP(selectedNote: Note?) {
        _uiState.update { currentState ->
            currentState.copy(
                notes = currentState.notes,
                menu = !currentState.menu,
                selectedNote = selectedNote
            )
        }
    }

    fun delete() {
        GlobalScope.launch(Dispatchers.IO) {
            if (_uiState.value.selectedNote != null) {
                repository.deleteFromRoom(_uiState.value.selectedNote!!)
                openOrClosePopUP(null)
            }
        }
    }
}