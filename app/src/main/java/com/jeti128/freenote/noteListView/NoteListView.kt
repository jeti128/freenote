package com.jeti128.freenote.noteListView

import android.content.Intent
import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.Popup
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.jeti128.freenote.R
import com.jeti128.freenote.noteView.NoteViewModel
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun NoteListView(navController: NavController, nlvm: NoteListViewModel = viewModel()) {
    val states by nlvm.uiState.collectAsState();

    nlvm.getAll()
    val context = LocalContext.current

    if (states.menu) {
        AlertDialog(
            onDismissRequest = { nlvm.openOrClosePopUP(null) },
            title = { Text("Menu") },
            text = {
                Column(modifier = Modifier.fillMaxWidth()) {
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .clickable {
                                nlvm.delete();
                            }
                            .padding(bottom = 15.dp)
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.delete),
                            contentDescription = "Back buttom",
                            colorFilter = ColorFilter.tint(Color.Black),
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .padding(start = 0.dp, end = 10.dp, top = 0.dp, bottom = 0.dp)
                                .size(20.dp)
                        )
                        Text(
                            text = "Delete",
                        )

                    }
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .clickable {
                                val sendIntent: Intent = Intent().apply {
                                    action = Intent.ACTION_SEND
                                    putExtra(Intent.EXTRA_TEXT, states.selectedNote?.note)
                                    type = "text/plain"
                                }

                                val shareIntent = Intent.createChooser(sendIntent, null)
                                context.startActivity(shareIntent)
                            }
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.share),
                            contentDescription = "Back buttom",
                            colorFilter = ColorFilter.tint(Color.Black),
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .padding(start = 0.dp, end = 10.dp, top = 0.dp, bottom = 0.dp)
                                .size(20.dp)
                        )
                        Text(
                            text = "Share",
                        )

                    }
                }
            },
            modifier = Modifier // Set the width and padding
                .fillMaxWidth()
                .padding(12.dp),
            shape = RoundedCornerShape(5.dp),
            backgroundColor = Color.White,
            properties = DialogProperties(
                dismissOnBackPress = true,
                dismissOnClickOutside = true
            ),
            confirmButton = {
            },
            dismissButton = {
            },
        )
    }

    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        items(items = states.notes ?: listOf(), itemContent = { item ->
            var substringlength: Int = 40;
            var date = (item.update.month + 1).toString() + "." + item.update.getDate().toString();
            if (item.update.month < 10) {
                date = "0" + date
            }
            if (item.note.length < 40) {
                substringlength = item.note.length;
            }
            LazyRow(modifier = Modifier
                .combinedClickable(
                    onClick = { navController.navigate("note?id=${item.id}") },
                    onLongClick = { nlvm.openOrClosePopUP(item) }
                )
                .fillMaxWidth(),
            ) {
                item() {
                    Text(
                        item.note.substring(0, substringlength),
                        modifier = Modifier
                            .fillParentMaxWidth(0.85f)
                            .padding(start = 17.dp, top = 15.dp, bottom = 10.dp),
                            //.clickable { navController.navigate("note?id=${item.id}") },
                        fontWeight = FontWeight.Bold,
                    )
                }
                item {
                    Text(text = date, modifier = Modifier.padding(top = 5.dp))
                }
            }
            TabRowDefaults.Divider(color = Color.LightGray)
        });
    }

}