package com.jeti128.freenote.noteListView

import androidx.compose.runtime.mutableStateListOf
import com.jeti128.freenote.db.Note

data class NoteListViewState(
    val notes: List<Note> = mutableStateListOf(),
    val menu: Boolean = false,
    val selectedNote: Note? = null,
)