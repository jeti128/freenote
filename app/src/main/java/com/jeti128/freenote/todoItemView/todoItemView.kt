package com.jeti128.freenote.todoItemView

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Button
import androidx.compose.material.Checkbox
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController

@Composable
fun todoItemView(
    navController: NavController,
    todoRootId: Int,
    category: String,
    updateTodo: suspend () -> Unit,
    tivm: TodoItemViewModel = viewModel()
) {
    val states by tivm.uiState.collectAsState();

    if (states.root == 0 && states.root != todoRootId) {
        tivm.get(todoRootId, category, updateTodo);
    }

    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        itemsIndexed(items = states.ids ?: listOf(), itemContent = { index, item ->
            Row(modifier = Modifier.fillMaxWidth()) {
                OutlinedTextField(
                    value = states.title[index],
                    label = { Text("Title") },
                    onValueChange = { newText ->
                        tivm.updateTitleField(index, newText)
                    },
                    modifier = Modifier
                        .fillMaxWidth(0.9f)
                        .padding(start = 5.dp, end = 5.dp, top = 5.dp, bottom = 5.dp)
                )
                Checkbox(
                    checked = states.status[index],
                    onCheckedChange = { newValue -> tivm.updateCheckboxField(index, newValue) },
                    modifier = Modifier
                        .padding(start = 5.dp, end = 5.dp, top = 17.dp, bottom = 7.dp)
                )
            }
        })
        item {
            Text(text = "+ Add new item", textAlign = TextAlign.Center,
                modifier = Modifier
                    .clickable(onClick = { tivm.addNew() })
                    .fillMaxWidth()
                    .padding(bottom = 300.dp, top = 10.dp)
            )
        }
    }
}