package com.jeti128.freenote.todoItemView

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.ui.text.input.TextFieldValue

data class todoItemViewState(
    val root: Int = 0,
    val category: String = "",
    val ids: List<Int> = mutableStateListOf(),
    val title: List<TextFieldValue> = mutableStateListOf(),
    val status: List<Boolean> = mutableStateListOf(),
)
