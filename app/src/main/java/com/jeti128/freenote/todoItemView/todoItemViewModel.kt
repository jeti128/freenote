package com.jeti128.freenote.todoItemView

import com.jeti128.freenote.todoView.todoViewState

import android.app.Application
import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.*
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.jeti128.freenote.db.*
import com.jeti128.freenote.noteView.NoteViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class TodoItemViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ItemRepository;
    private var updateTodoFun: (suspend () -> Unit)?;
    private val _uiState = MutableStateFlow(
        todoItemViewState()
    )
    val uiState: StateFlow<todoItemViewState> = _uiState.asStateFlow()

    private var loading: Boolean = false;

    init {
        val itemDoa = AppDB.getDatabase(application).ItemDBDao();
        repository = ItemRepository(itemDoa);
        updateTodoFun = null
    }

    fun get(
        id: Int,
        category: String,
        updateTodo: suspend () -> Unit,
    ) {
        updateTodoFun = updateTodo;
        _uiState.update { currentState ->
            currentState.copy(
                root = id,
                category = category,
                ids = currentState.ids,
                title = currentState.title,
                status = currentState.status
            )
        }
        loading = true;
        viewModelScope.launch(Dispatchers.IO) {
            repository.getItemsToMain(category, id).collect { items ->
                Log.d("count items", items.size.toString())
                if (loading) {
                    loading = false;
                    var ids = mutableStateListOf<Int>();
                    var titles = mutableStateListOf<TextFieldValue>();
                    var status = mutableStateListOf<Boolean>();
                    for (item in items) {
                        ids.add(item.id)
                        titles.add(TextFieldValue(item.title))
                        status.add(item.status)
                    }
                    _uiState.update { currentState ->
                        currentState.copy(
                            root = id,
                            category = category,
                            ids = ids,
                            title = titles,
                            status = status
                        )
                    }
                }
            }
        }
    }

    fun updateTitleField(index: Int, newValue: TextFieldValue) {
        var titles = _uiState.value.title.toMutableStateList();
        titles[index] = newValue;
        _uiState.update { currentState ->
            currentState.copy(
                root = currentState.root,
                category = currentState.category,
                ids = currentState.ids,
                title = titles,
                status = currentState.status
            )
        }

        viewModelScope.launch(Dispatchers.IO) {
            save(index);
        }
    }

    fun updateCheckboxField(index: Int, newValue: Boolean) {
        var status = _uiState.value.status.toMutableStateList();
        status[index] = newValue;
        _uiState.update { currentState ->
            currentState.copy(
                root = currentState.root,
                category = currentState.category,
                ids = currentState.ids,
                title = currentState.title,
                status = status
            )
        }

        viewModelScope.launch(Dispatchers.IO) {
            save(index);
        }
    }

    fun addNew() {
        var ids = _uiState.value.ids.toMutableStateList();
        var titles = _uiState.value.title.toMutableStateList();
        var status = _uiState.value.status.toMutableStateList();

        ids.add(0);
        titles.add(TextFieldValue(""));
        status.add(false);

        _uiState.update { currentState ->
            currentState.copy(
                root = currentState.root,
                category = currentState.category,
                ids = ids,
                title = titles,
                status = status
            )
        }
    }

    suspend fun save(index: Int) {
        updateTodoFun?.let { it() };

        var item: Item = Item(
            id = _uiState.value.ids[index],
            category = _uiState.value.category,
            root = _uiState.value.root,
            title = _uiState.value.title[index].text,
            status = _uiState.value.status[index]
        );

        if (item.id == 0) {
            var newItemId = repository.insert(item);
            var ids = _uiState.value.ids.toMutableStateList();
            ids[index] = newItemId.toInt();

            _uiState.update { currentState ->
                currentState.copy(
                    root = currentState.root,
                    category = currentState.category,
                    ids = ids,
                    title = currentState.title,
                    status = currentState.status
                )
            }
        } else {
            repository.updateInRoom(item);
        }
    }
}