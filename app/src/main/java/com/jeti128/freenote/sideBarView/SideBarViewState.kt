package com.jeti128.freenote.sideBarView

data class SideBarViewState (
    val logined: Boolean = false,
    val firstName: String = "Janovicz",
    val lastName: String = "Gábor"
)