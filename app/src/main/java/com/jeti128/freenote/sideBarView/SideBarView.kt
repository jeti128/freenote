package com.jeti128.freenote.sideBarView

import android.content.ClipData.Item
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.Text
import com.jeti128.freenote.R
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.jeti128.freenote.MainView.MenuItem

data class SidMenuItems(
    val name: String = "",
    val route: String = "/",
    val icon: Int = R.drawable.icon_menu,
)

@Composable
fun SideBarView(navController: NavController, sbvm: SideBarViewModel = viewModel()) {
    val menuItems: MutableList<SidMenuItems> = mutableListOf(
        SidMenuItems("Notes", "notelist", R.drawable.note),
        SidMenuItems(
            "Todos", "todolist", R.drawable.checklist
        ),
    )
    val states by sbvm.uiState.collectAsState();

    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        item() {
            LazyRow() {
                item() {
                    if (!states.logined) {
                        Image(
                            painter = painterResource(id = R.drawable.account_circle),
                            contentDescription = "User Avatar",
                            colorFilter = ColorFilter.tint(MaterialTheme.colors.primaryVariant),
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier
                                .padding(start = 10.dp, end = 25.dp, top = 10.dp, bottom = 5.dp)
                                .size(40.dp)
                        )
                    } else {
                        // TODO implement logined component
                    }
                }
                item() {
                    if (!states.logined) {
                        Text(text = "Login", modifier = Modifier.padding(top = 15.dp))
                        Text(text = " / ", modifier = Modifier.padding(top = 15.dp))
                        Text(text = "Singup", modifier = Modifier.padding(top = 15.dp))
                    } else {
                        Text(text = states.firstName, modifier = Modifier.padding(top = 10.dp))
                        Text(text = " ", modifier = Modifier.padding(top = 10.dp))
                        Text(text = states.lastName, modifier = Modifier.padding(top = 10.dp))
                    }
                }
            }
            TabRowDefaults.Divider(color = Color.LightGray, modifier = Modifier.padding(top = 20.dp))
        }
        items(items = menuItems ?: listOf(), itemContent = { item ->
            LazyRow(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable { navController.navigate(item.route) }) {
                item() {
                    Image(
                        painter = painterResource(id = item.icon),
                        contentDescription = item.name,
                        colorFilter = ColorFilter.tint(MaterialTheme.colors.primaryVariant),
                        contentScale = ContentScale.FillWidth,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 25.dp, top = 10.dp, bottom = 5.dp)
                            .size(40.dp)
                    )
                }
                item() {
                    Text(text = item.name, modifier = Modifier.padding(top = 15.dp))
                }
            }
        })
    }

}