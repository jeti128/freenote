package com.jeti128.freenote.sideBarView

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jeti128.freenote.MainView.MainViewState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class SideBarViewModel(application: Application) : AndroidViewModel(application) {
    private val _uiState = MutableStateFlow(
        SideBarViewState()
    )
    val uiState: StateFlow<SideBarViewState> = _uiState.asStateFlow()

    init {
    }

}