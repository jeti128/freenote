package com.jeti128.freenote.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val materialBlue700 = Color(0xFF1976D2)
val dark200 = Color(0xFF222222)
val gray500 =  Color( 0xff333333);
val white500 = Color(0xffeeeeee)